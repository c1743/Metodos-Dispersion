package metodosdispersion;

/**
 *
 * @author AndresFWilT
 */
public class Transformacion extends Metodos {

    private int base, tArreglo;

    public Transformacion(int llave, int base, int tArreglo) {
        super(llave);
        this.base = base;
        this.tArreglo = tArreglo;
    }

    public String getDispersion() {
        String cam = Integer.toString(super.getLlave(), base);
        String bin = "";
        for (int i = 0; i < cam.length(); i++) {
            bin += Integer.toBinaryString(Integer.parseInt(cam.charAt(i) + "", base));
        }
        int i = Integer.parseInt(bin, 2);
        return (i % tArreglo) + "";
    }

}
