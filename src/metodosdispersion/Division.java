package metodosdispersion;

/**
 *
 * @author AndresFWilT
 */
public class Division extends Metodos {

    private int nPrimo;

    public Division(int llave, int nPrimo) {
        super(llave);
        this.nPrimo = nPrimo;
    }

    public String getDispersion() {
        return String.valueOf(super.getLlave() % nPrimo);
    }

}
