package metodosdispersion;

/**
 *
 * @author AndresFWilT
 */
public class MidSquare extends Metodos {

    private int nBits;

    public MidSquare(int llave, int nBits) {
        super(llave);
        this.nBits = nBits;
    }

    public String getDispersion() {
        int tamaño, mitadBits;
        long numero = super.getLlave();
        System.out.println("Numero: " + numero * numero);
        String bits = Long.toBinaryString(numero * numero);
        System.out.println("Bits: " + bits);
        int añadir;
        if (bits.length() % 2 == 0) {
            tamaño = bits.length() / 2;
            añadir = 0;
        } else {
            tamaño = (bits.length() / 2) + 1;
            añadir = 0;
        }
        if (nBits % 2 == 0) {
            mitadBits = nBits / 2;
        } else {
            mitadBits = (nBits / 2) + 1;
        }
        String bitsCentrales = bits.substring(tamaño - mitadBits + añadir, tamaño - mitadBits + nBits + añadir);
        return String.valueOf((int) convertirADecimal(bitsCentrales));
    }

    private static double convertirADecimal(String bits) {
        double valor = 0;
        for (int i = 0; i < bits.length(); i++) {
            if (bits.charAt(i) == '1') {
                valor += Math.pow(2, bits.length() - 1 - i);
            }
        }
        return valor;
    }

}
